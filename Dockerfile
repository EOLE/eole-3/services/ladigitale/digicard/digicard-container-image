FROM hub.eole.education/proxyhub/alpine:latest as BUILDER
RUN apk add npm git
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digicard/digicard-sources.git && \
    cd digicard-sources && \
    git checkout $(cat ../app-version)
WORKDIR /tmp/digicard-sources
RUN npm install
RUN npm run build
FROM hub.eole.education/proxyhub/library/nginx:alpine
COPY --from=BUILDER /tmp/digicard-sources/dist /usr/share/nginx/html
COPY server_tokens.conf /etc/nginx/conf.d/
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx
EXPOSE 80

